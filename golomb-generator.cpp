#include <getopt.h>
#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char *argv[]) {
  int n = 0;
  if(argc!=2) {
    cerr << "Usage: " << argv[0] << " n" << endl;
  } else {
    n = atoi(argv[1]);
    cerr << "Generating conflict graph for n = " << n << endl;
  }

  for(int a = 0; a <= n; ++a)
    for(int b = a + 1; b <= n; ++b)
      for(int c = b; c <= n; ++c)
	for(int d = c + 1; d <= n; ++d) {
	  if(abs(a-b)==abs(c-d)) {
	    if(b!=c) cout << a << " " << b << " " << c << " " << d << endl;
	    else cout << a << " " << b << " " << d << endl;
	  }
	}
}
