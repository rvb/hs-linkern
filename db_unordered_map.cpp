#include <iterator>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <cassert>
#include <boost/functional/hash.hpp>
#include "Hypergraph.hpp"
#include "db_umap.hpp"

template <typename Container> // we can make this generic for any container [1]
struct container_hash {
    std::size_t operator()(Container const& c) const {
        return boost::hash_range(c.begin(), c.end());
    }
};

typedef unordered_map<Edge, ULeaf, container_hash<Edge> > DB;
DB db;
int maxverts;

bool ULeaf::used(Vertex n) {
  unordered_set<Vertex>::iterator i = usedb.find(n);
  if(i == usedb.end()) return false;
  else return true;
}

void ULeaf::setused(Vertex n) {
  usedb.insert(n);
}

Leaf* lookup(const Edge &e) {
  DB::iterator i = db.find(e);
  if(i == db.end())
    i = db.insert(pair<Edge, ULeaf>(e, ULeaf(maxverts + 1))).first;
  return &i->second;
}

void initialize_db(const Hypergraph &G, const Graphstats &s) {
  maxverts = s.vertices;
}
