#include <cassert>
#include <cstdlib>
#include "Hypergraph.hpp"

extern Vertex last;

void* new_children(size_t s) {
  void* r = calloc(last + 1, s);
  assert(r);
  return r;
}
