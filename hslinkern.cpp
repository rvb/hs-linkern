#include <iostream>
#include <getopt.h>
#include <cstdlib>
#include <cmath>
#include <iterator>
#include <functional>
#include <fstream>
#include "config.h"
#include "bitops.hpp"
#include "Hypergraph.hpp"
#include "db.hpp"

using namespace std;

void stats_time(const char* heading, const char* secstr, const function<void ()> &func) {
  cout << endl << "---- " << heading << " ----" << endl;
    clock_t t = clock();
    func();
    t = clock() - t;
    cout << " + " << secstr << ": " << ((float)t)/CLOCKS_PER_SEC << endl;
}

void usage(const char *th) {
  cout << hslinkern_NAME <<", (C) 2011-2016 Rene van Bevern <rvb@nsu.ru>" << endl
       << "-------------------------------------------------------------------------" << endl;
  cout << "Usage: " << th << " [options] -f hypergraph_file.txt" << endl;
  cout << endl << "Valid options are" << endl;
  cout << "-k K: kernelize the instance asking for a hitting set of size K." << endl;
  cout << "      Per default, an upper bound on the minimum hitting set size" << endl;
  cout << "      is used as K." << endl;
  cout << "-o OUTPUT: write kernelized hypergraph into the file OUTPUT." << endl;
  cout << "           By default, no output is generated, only statistics." << endl;
  exit(1);
}    

struct petals_less_than {
  bool yes;
  unsigned int k;

  petals_less_than(int _k) : k(_k) { yes = true; }
  void operator()(const Edge &c) {
    if(lookup(c)->petals > k) yes = false;
  }
};

struct add_petal {
  const Edge &e;
  add_petal(const Edge &_e) : e(_e) {}
  void operator()(const Edge &c) {
    Leaf *l(lookup(c));
    Edge enotc(edge_subtraction(e, c));
    
    for(Edge::const_iterator i = enotc.begin(); i != enotc.end(); ++i)
      if(l->used(*i)) return;

    l->petals++;

    for(Edge::const_iterator i = enotc.begin(); i != enotc.end(); ++i)
      l->setused(*i);
  }
};

Hypergraph kernelize(const Hypergraph &G, const Graphstats &s, unsigned int k) {
  Hypergraph H;

  cout << endl << "... computing kernel for k = " << k << " ..." << endl;
  
  for(Hypergraph::const_iterator i = G.begin(); i != G.end(); ++i) {
    petals_less_than lt(k);
    foreach_intersection(G, *i, s, lt);
    if(lt.yes) {
      add_petal addp(*i);
      H.push_back(*i);
      foreach_intersection(G, *i, s, addp);
      lookup(*i)->petals = k+1;
    }

    if(lookup(Edge())->petals > k) {
      cout << "No-instance" << endl;
      exit(EXIT_SUCCESS);
    }
  }
  return H;
}

void print_stats(const Graphstats &stats, const char* prefix) {
  cout << prefix << " "<< "Edges              : " << stats.edges << endl;
  cout << prefix << " "<< "Largest vert index : " << stats.vertices << endl;
  cout << prefix << " "<< "Maximum edge size  : " << stats.edgesize << endl;
  cout << prefix << " "<< "Smallest huge edge : " << stats.hugesize << endl;
  cout << prefix << " "<< "Huge edges         : " << stats.hugeedges << endl;
}

int main(int argc, char *argv[]) {
  int c;
  char* f = 0;
  int k = -1;
  ofstream of;
  
  while((c=getopt(argc, argv, "f:k:o:")) != -1)
    switch (c) {
    case 'k': {
      k = atoi(optarg);
      assert(k > 0);
      break;
    }
    case 'o': {
      of.open(optarg);
      break;
    }
    case 'f': {
      f = optarg;
      break;
    }
    default: usage(argv[0]);
    }
  
  Hypergraph G, H;
  Graphstats stats;

  if(f) 
    stats_time("Reading hypergraph", "read seconds", [&] () {
	G = read_hypergraph(f);
      });
  else {
    cout << "Error: no hypergraph to kernelize" << endl;
    usage(argv[0]);
  }
  
  stats_time("Graph stats", "stats seconds", [&] () {
      stats = get_stats(G);
      print_stats(stats, "< input ");
    });

  stats_time("Getting bounds", "bounds seconds", [&] () {
      edges_by_size(G, stats);
      Hypergraph apx(approx_hs(G, stats));
  
      int apxbound = get_stats(apx).edgesums;
      int greedybound = greedy_hs(G, stats);
      int vertcount = verts(G, stats).size();
      cout << "< input  # of verts : " << vertcount << endl;


      int golbound = vertcount - sqrt(vertcount);
      
      if(k < 0) {
	k = apxbound;
	if(greedybound < k) k = greedybound;
	if(golbound < k) k = golbound;
      }
      
      cout << "Upper bound HS (APX) : " << apxbound << endl;
      cout << "Upper bound HS (grd) : " << greedybound << endl;
      cout << "Upper bound HS (gol) : " << golbound << endl;
      cout << "Lower bound HS       : " << apx.size() << endl;
    });

  stats_time("Kernelizing...", "kernel seconds", [&] () {
      initialize_db(G, stats);
      H = kernelize(G, stats, k);
    });

  stats_time("Kernel stats", "kstats seconds", [&] () {
      Graphstats hstats(get_stats(H));
      cout << endl << "Kernel statistics" << endl;
      print_stats(hstats, "> output");
      int vertcount = verts(H, hstats).size();
      cout << "> output # of verts      : " << vertcount << endl;
    });

  if(of.is_open()) {
    for(Hypergraph::iterator i = H.begin(); i != H.end(); ++i) {
      copy(i->begin(), i->end(), ostream_iterator<Vertex>(of, " "));
      of << endl;
    }
    of.close();
  }
  return 0;
}
