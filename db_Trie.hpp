#ifndef _DB_UMAP_HPP
#define _DB_UMAP_HPP

#include <Hypergraph.hpp>
#include <db.hpp>

class TLeaf : public Leaf {
public:
  virtual bool used(Vertex n);
  virtual void setused(Vertex n);
  void setunused(Vertex n);
  Vertex *usedb;
};

void initialize_db(const Hypergraph &, const Graphstats &);
Leaf* lookup(const Edge &);

#endif
