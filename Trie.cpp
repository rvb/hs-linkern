#include <iterator>
#include <iostream>
#include <algorithm>
#include "Hypergraph.hpp"
#include "db_Trie.hpp"

struct Trie {
  TLeaf* leaf;
  union {
    Trie* children;
    Hypergraph* to_create;
  };
};

Trie t;

bool TLeaf::used(Vertex n) { return(usedb[n]); }
void TLeaf::setused(Vertex n) { usedb[n]=1; }
void TLeaf::setunused(Vertex n) { usedb[n]=0; }

void initialize_worker(Edge::size_type level, Trie &t, const Graphstats &s) {
  Hypergraph* H = t.to_create;
  Vertex last = -1;
  list<Vertex> init_nodes;
  
  t.leaf = 0;
  for(Hypergraph::iterator i = H->begin(); i != H->end(); ++i)
    if(level >= i->size()) { 
      t.leaf = new TLeaf;
      t.leaf->usedb = (Vertex*)malloc(sizeof(Vertex)*(s.vertices + 1));
    }
    else{
      last = max(last, (*i)[level]);
      init_nodes.push_back((*i)[level]);
    }

  if(init_nodes.empty()) return;
  
  t.children = (Trie*)malloc(sizeof(Trie)*(last + 1));

  bool* dupes = new bool[last + 1];
  for(list<Vertex>::iterator v = init_nodes.begin(); v != init_nodes.end(); ++v)
    dupes[*v] = false;

  list<Vertex>::iterator v = init_nodes.begin();
  while(v!=init_nodes.end()) {
    if(!dupes[*v]) {
      dupes[*v] = 1;
      ++v;
    } else v = init_nodes.erase(v);
  }

  delete[] dupes;
  
  for(list<Vertex>::iterator v = init_nodes.begin(); v != init_nodes.end(); ++v)
    t.children[*v].to_create = new Hypergraph;
 
  Hypergraph::iterator i = H->begin();
  while(i != H->end()) {
    if(level < i->size()) t.children[(*i)[level]].to_create->push_back(*i);
    i = H->erase(i);
  }

  if(level > 0) delete H;

  for(list<Vertex>::iterator v = init_nodes.begin(); v != init_nodes.end(); ++v)
    initialize_worker(level + 1, t.children[*v], s);
}

Leaf* lookup(const Edge &e) {
  Trie *cur = &t;
  for(Edge::const_iterator i = e.begin(); i != e.end(); ++i)
    cur = &cur->children[*i];
  assert(cur->leaf);
  return cur->leaf;
}

struct agglomerator {
  Hypergraph cores;
  void operator()(const Edge &c) { cores.push_back(c); }
};

struct set_unused {
  const Edge& e;
  set_unused(const Edge &_e) : e(_e) {}
  void operator()(const Edge &c) {
    TLeaf *l = (TLeaf*)(lookup(c));
    l->petals = 0;
    for(Edge::const_iterator i = e.begin(); i != e.end(); ++i)
      l->setunused(*i);
  }
};

void initialize_db(const Hypergraph &G, const Graphstats &s) {
  agglomerator agglomerate;
  for(Hypergraph::const_iterator i = G.begin(); i != G.end(); ++i)
    foreach_intersection(G, *i, s, agglomerate);
  cout << "Initializing Trie for " << agglomerate.cores.size() << " cores" << endl;

  t.to_create = &agglomerate.cores;
  initialize_worker(0, t, s);

  for(Hypergraph::const_iterator i = G.begin(); i != G.end(); ++i) {
    set_unused f(*i);
    foreach_intersection(G, *i, s, f);
  }
}
