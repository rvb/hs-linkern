Linear-time data reduction for _d_-Hitting Set
==============================================

This is a linear-time data reduction preprocessor for the _d_-Hitting Set problem:

**Input:** A universe _U_, a collection _C_ of subsets of cardinality at
  most _d_ of _U_, and a natural number _k_.     
**Output:** A subset _S_ of at most _k_ elements of _U_ that contains at
  least one element of each set in _C_, if such a subset _S_ exists.
  
The program outputs a _d_-Hitting Set instance such that _C_ contains at
most O(_k^d_) subsets and admits a solution of size _k_ if and only if
the input instance does, that is, the data reduction algorithm does not
change the size of an optimal solution.

The problem has applications, among others, in radio frequency
allocation and race condition detection of parallel programs at runtime.
The applications and the data reduction algorithm are described in
Chapter 5 of

- [René van Bevern (2014).  Fixed-parameter linear-time algorithms for
  graph and hypergraph problems arising in industrial applications.
  Volume 1 in Foundations of Computer Science, TU Berlin University
  Press](http://dx.doi.org/10.14279/depositonce-4131)

and

- [René van Bevern (2014).  Towards optimal and expressive kernelization
  for _d_-hitting set. _Algorithmica_,
  70(1):129–147](http://dx.doi.org/10.1007/s00453-013-9774-3)

Downloading and compiling
-------------------------

Just download a [Zip archive](https://gitlab.com/rvb/hs-linkern/repository/archive.zip?ref=master) or [tar.gz archive](https://gitlab.com/rvb/hs-linkern/repository/archive.tar.gz?ref=master) from Github or clone the project using

```
git clone https://gitlab.com/rvb/hs-linkern.git
```

The source code was tested to compile on Windows 7 with Cygwin and on Debian and Fedora GNU/Linux with

 * Boost 1.38
 * CMake 2.8
 * G++ 4.7

To compile, run

```
cmake CMakeLists.txt
make
```

Usage:
------

After compiling, there will be several executables whose name starts
with `hslinkern_`.  These are implementations of the algorithm using as
data structure the _malloc_tree_, _calloc_tree_, _hash_table_, and
_balanced_tree_ described in the above works, respectively.

If memory shortage is no problem, we recommend using `hslinkern_calloc`
since it is the fastest.  If memory is insufficient, we recommend using
`hslinkern_balanced` as a good compromise between speed and memory
usage.

To get usage instructions, run

```
./hslinkern_balanced --help
```

Hypergraph format:
------------------
The input format has on each line a hyperedge, which consists of
vertex numbers. An example hypergraph can be generated using
```
./golomb_generator 10
```
This generates a GOLOMB SUBRULER conflict graph on 10 vertices as
described in the above works.

----

René van Bevern <rvb@nsu.ru>
