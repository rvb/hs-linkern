#include <cstdlib>
#include <sys/mman.h>
#include "Hypergraph.hpp"
#include <cerrno>
#include <cstring>

extern Vertex last;

void* new_children(size_t s) {
  void* r = mmap(0, s * (last + 1),
		 PROT_READ | PROT_WRITE,
		 MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
  if(r == MAP_FAILED) {
    cout << strerror(errno) << endl;
    abort();
  }
  return r;
}
