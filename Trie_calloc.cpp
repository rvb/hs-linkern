#include <iterator>
#include <iostream>
#include <algorithm>
#include <sys/mman.h>
#include <cerrno>
#include <cstring>
#include "Hypergraph.hpp"
#include "db_Trie.hpp"

struct Trie {
  TLeaf* leaf;
  Trie* children;
};

Trie t;
Vertex last;

bool TLeaf::used(Vertex n) { return(usedb[n]); }
void TLeaf::setused(Vertex n) { usedb[n]=1; }
void TLeaf::setunused(Vertex n) { usedb[n]=0; }

void* new_children(size_t s) {
  void* r = (Trie*)calloc(last + 1, s);
  assert(r);
  return r;
}

Leaf* lookup(const Edge &e) {
  Trie *cur = &t;
  for(Edge::const_iterator i = e.begin(); i != e.end(); ++i) {
    if(!cur->children) cur->children = (Trie*)new_children(sizeof(Trie));
    cur = &cur->children[*i];
  }
  if(!cur->leaf) {
    cur->leaf = new TLeaf;
    cur->leaf->petals = 0;
    cur->leaf->usedb = (Vertex*)new_children(sizeof(Vertex));
  }
  return cur->leaf;
}

void initialize_db(const Hypergraph &G, const Graphstats &s) {
  t.leaf = 0;
  t.children = 0;
  last = s.vertices;
}
