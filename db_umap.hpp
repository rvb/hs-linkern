#ifndef _DB_UMAP_HPP
#define _DB_UMAP_HPP

#include <unordered_set>
#include <Hypergraph.hpp>
#include <db.hpp>

class ULeaf : public Leaf {
public:
  explicit ULeaf(int size) : usedb(size) {};
  virtual bool used(Vertex n);
  virtual void setused(Vertex n);
private:
  unordered_set<Vertex> usedb;
};

void initialize_db(const Hypergraph &, const Graphstats &);
Leaf* lookup(const Edge &);

#endif
